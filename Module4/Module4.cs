﻿using System;
using System.Linq;
using System.Text;

namespace M4
{
    public class Module4
    {
        static void Main(string[] args)
        {
            //Task 1_A
            int[] array = { 50, 151, 5, 6, 26, 136, 99 };
            
            Module4 module4 = new Module4();

            int maximumElement = module4.Task_1_A(array);

            Console.WriteLine("Maximum element is: {0}", maximumElement);

            //Task 1_B
            int minimumElement = module4.Task_1_B(array);

            Console.WriteLine("Minimum element is: {0}", minimumElement);

            //Task 1_C
            int sumOfElements = module4.Task_1_C(array);

            Console.WriteLine("Sum of array elements is: {0}", sumOfElements);

            //Task 1_D
            int maxAndMinDifference = module4.Task_1_D(array);
            Console.WriteLine("Maximum and minimum elements difference is: {0}", maxAndMinDifference);

            //Task 1_E
            module4.Task_1_E(array);

            //Task 2_Three_integer_argsuments
            int integerSum = module4.Task_2(2,2,2);
            Console.WriteLine("Sum of three integer arguments is: {0}", integerSum);

            //Task 2_Two_integer_argsuments
            integerSum = module4.Task_2(2, 2);
            Console.WriteLine("Sum of two integer arguments is: {0}", integerSum);

            //Task 2_Three_double_argsuments
            double doubleSum = module4.Task_2(2.0, 2.3, 2.5);
            Console.WriteLine("Sum of three double arguments is: {0}", doubleSum);

            //Task 2_Two_string_argsuments
            string stringSum = module4.Task_2("Hello ", "world!");
            Console.WriteLine("Concatenation of two string arguments is: {0}", stringSum);

            //Task 2_Two_integer_array_argsuments
            int[] biggestArray = { 5, 5, 5, 5, 5 };
            int[] smallestArray = { 1, 1, 1, 1 };
            int[] arraySum = module4.Task_2(biggestArray, smallestArray);

            Console.Write("This is sum of two integer arrays: ");
            for (int i = 0; i < arraySum.Length; i ++)
            {
                Console.Write("{0}, ", arraySum[i]);
            }
            Console.WriteLine();

            //Task 3_A
            int a = 5;
            int b = 15;
            int c = 20;

            Console.WriteLine($"a value is: {a}, b value is: {b}, c value is: {c}");
            
            module4.Task_3_A(ref a, ref b, ref c);
            
            Console.WriteLine($"a new value is: {a}, b new value is: {b}, c new value is: {c}");

            //Task 3_B
            double radius = 55;
            double length = 0;
            double square = 0;

            Console.WriteLine($"length value is: {length}, square value is: {square}");

            module4.Task_3_B(radius, out length, out square);

            Console.WriteLine($"length new value is: {length}, square value is: {square}");

            //Task 3_C
            int[] array2 = { 50, 151, 5, 6, 26, 136, 99 };

            module4.Task_3_C(array2, out int maxItem, out int minItem, out int sumOfItems);

            Console.WriteLine($"maximum item of array is: {maxItem}, minimum item of array is: {minItem}, sum of array items is: {sumOfItems}");

            //Task 4_A
            (int, int, int) numbers = (5, 15, 20);

            Console.WriteLine($"first argument of tuple is: {numbers.Item1}, second argument of tuple is: {numbers.Item2}, third argument of tuple is: {numbers.Item3}");

            (int, int, int) task_4_A_result = module4.Task_4_A(numbers);

            Console.WriteLine($"updated first argument of tuple is: {task_4_A_result.Item1}, updated second argument of tuple is: {task_4_A_result.Item2}, updated third argument of tuple is: {task_4_A_result.Item3}");

            //Task 4_B
            (double, double) task_4_B_result = module4.Task_4_B(radius);

            Console.WriteLine($"length new value is: {task_4_B_result.Item1}, square value is: {task_4_B_result.Item2}");

            //Task 4_C
            (int, int, int) task_4_C_result = module4.Task_4_C(array2);

            Console.WriteLine($"maximum item of array is: {task_4_C_result.Item2}, minimum item of array is: {task_4_C_result.Item1}, sum of array items is: {task_4_C_result.Item3}");

            //Task 5
            int[] array3 = { 1, 1, 1, 1, 1 };

            module4.Task_5(array3);

            //Task 6

            int[] array4 = { 1, 2, 3, 4, 5 };

            module4.Task_6(array4, SortDirection.Ascending);

            module4.Task_6(array4, SortDirection.Descending);

            //Task 7
            double x1 = 5.5;
            double x2 = 10.3;
            double e = 0.01;
            double result = 0.0;

            Func<double, double> func = Module4.Divide;
            result = module4.Task_7(func, x1, x2, e, result);

            Console.WriteLine("x value is: {0}", result);

        }


        public int Task_1_A(int[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException(nameof(array));
            }

            int maximumElement = array.Max();

            return maximumElement;
        }

        public int Task_1_B(int[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException(nameof(array));
            }

            return array.Min();
        }

        public int Task_1_C(int[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException(nameof(array));
            }

            return array.Sum();
        }

        public int Task_1_D(int[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException(nameof(array));
            }

            int difference = array.Max() - array.Min();

            return difference;
        }

        public void Task_1_E(int[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException(nameof(array));
            }

            int minimumElement = array.Min();
            int maximumElementt = array.Max();

            for(int i = 0; i < array.Length; i ++)
            {
                if(i % 2 == 0)
                {
                    array[i] += maximumElementt;
                }
                else
                {
                    array[i] -= minimumElement;
                }
            }
        }

        public int Task_2(int a, int b, int c)
        {
            int sum = a + b + c;
            return sum;
        }

        public int Task_2(int a, int b)
        {
            int sum = a + b;
            return sum;
        }

        public double Task_2(double a, double b, double c)
        {
            double sum = a + b + c;
            return sum;
        }

        public string Task_2(string a, string b)
        {
            StringBuilder stringBuilder = new StringBuilder(a);
            string sum = stringBuilder.Append(b).ToString();
            return sum;
        }

        public int[] Task_2(int[] a, int[] b)
        {
            if (a == null || a.Length == 0)
            {
                throw new ArgumentNullException(nameof(a));
            }

            if (b == null || b.Length == 0)
            {
                throw new ArgumentNullException(nameof(b));
            }

            int[] smallestArray = (a.Length < b.Length) ? a : b;
            int[] biggestArray = (a.Length < b.Length) ? b : a;

            for(int i = 0; i < biggestArray.Length; i ++)
            {
                if(i < smallestArray.Length)
                {
                    biggestArray[i] += smallestArray[i];
                }
            }

            return biggestArray;
        }

        public void Task_3_A(ref int a, ref int b, ref int c)
        {
            a += 10;
            b += 10;
            c += 10;
        }

        public void Task_3_B(double radius, out double length, out double square)
        {
            if(radius < 0)
            {
                throw new ArgumentException($"Radius should be positive! Inputed radius value is: {radius}.");
            }

            length = (2 * Math.PI * radius);
            
            square = (Math.PI * Math.Pow(radius, 2));
        }

        public void Task_3_C(int[] array, out int maxItem, out int minItem, out int sumOfItems)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException(nameof(array));
            }

            maxItem = array.Max();
            minItem = array.Min();
            sumOfItems = array.Sum();
        }

        public (int, int, int) Task_4_A((int, int, int) numbers)
        {
            numbers.Item1 += 10;
            numbers.Item2 += 10;
            numbers.Item3 += 10;

            return numbers;
        }

        public (double, double) Task_4_B(double radius)
        {
            if (radius < 0)
            {
                throw new ArgumentException($"Radius should be positive! Inputed radius value is: {radius}.");
            }

            (double, double) result = ((2 * Math.PI * radius), (Math.PI * Math.Pow(radius, 2)));

            return result;
        }

        public (int, int, int) Task_4_C(int[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException(nameof(array));
            }

            (int, int, int) result = (array.Min(), array.Max(), array.Sum());

            return result;
        }

        public void Task_5(int[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException(nameof(array));
            }

            for(int i = 0; i < array.Length; i ++)
            {
                array[i] += 5;
            }
        }

        public void Task_6(int[] array, SortDirection direction)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException(nameof(array));
            }

            if(direction == SortDirection.Ascending)
            {
                Array.Sort(array);
            }
            else if(direction == SortDirection.Descending)
            {
                Array.Sort(array);
                Array.Reverse(array);
            }
            
        }        

        public  double Task_7(Func<double, double> func, double x1, double x2, double e, double result = 0)
        {
            result = (x1 + x2) / 2;

            if((func(x1) * func(result)) > 0)
            {
                x1 = result;
            }
            else
            {
                x2 = result;
            }

            if(Math.Abs(x2 - x1) > (2.0 * e))
            {
                result = Task_7(func, x1, x2, e, result);
            }

            return result;
        }

        public static double Divide(double x)
        {
            double result = x / 2;
            return result;
        }
    }
}
